'use strict';

var util = require('util');
var path = require('path');
var fs = require('fs');
var SQLite = require('sqlite3').verbose();
var Bot = require('slackbots');
var request = require('request');

var NixaBot = function Constructor(settings) {
    this.settings = settings;
    this.settings.name = this.settings.name || 'nixabot';
    this.user = null;
};

util.inherits(NixaBot, Bot);

NixaBot.prototype.run = function () {
    NixaBot.super_.call(this, this.settings);

    this.on('start', this._onStart);
    this.on('message', this._onMessage);
};

NixaBot.prototype._onStart = function () {
    this._loadBotUser();
    this._firstRunCheck();
};

NixaBot.prototype._loadBotUser = function () {
    var self = this;
    this.user = this.users.filter(function (user) {
        return user.name === self.name;
    })[0];
};


NixaBot.prototype._firstRunCheck = function () {
    var self = this;
    self._welcomeMessage();
};

NixaBot.prototype._welcomeMessage = function () {
    this.postMessageToChannel(this.channels[0].name, "Hi!" +
        "\n I'm " + this.name + "!",
        {as_user: true});
};

NixaBot.prototype._onMessage = function (message) {
    var self = this;
    if (this._isChatMessage(message) &&
        this._isChannelConversation(message) && !this._isFromNixaBot(message) &&
        this._isMentioningNixaBot(message)
    ) {
        var channel = this._getChannelById(message.channel);

        request('http://www.meteomedia.com/api/data/caqc0363', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                self.postMessageToChannel(channel.name, data.sevendays.periods[0].cdate + " " +
                    data.sevendays.periods[0].metric_feelsLike + data.sevendays.periods[0].metric_feelsLike_unit +
                     " " + data.sevendays.periods[0].itn, {as_user: true});
            }
        });

    }
};

NixaBot.prototype._isChatMessage = function (message) {
    return message.type === 'message' && Boolean(message.text);
};

NixaBot.prototype._isChannelConversation = function (message) {
    return typeof message.channel === 'string' &&
        message.channel[0] === 'C';
};

NixaBot.prototype._isFromNixaBot = function (message) {
    return message.user === this.user.id;
};

NixaBot.prototype._isMentioningNixaBot = function (message) {
    return message.text.toLowerCase().indexOf('nixabot') > -1 ||
        message.text.toLowerCase().indexOf(this.name) > -1;
};

NixaBot.prototype._getChannelById = function (channelId) {
    return this.channels.filter(function (item) {
        return item.id === channelId;
    })[0];
};

NixaBot.prototype.getMeteo = function () {

};

module.exports = NixaBot;
